# Minos (Formerly known as the Silica Interactive Code Review Tool) #

## How it all began ##

I find myself to be rather anal about the usage of revision control systems,
specifically Git. My biggest bugbears are;

* Using Git as if it's a centralised system.
* Publishing of non-essential branches (work-in-progress, feature, topic, and
  the like).
* Using system-facilitated pull requests as inter-team code / peer reviews.

The observant among you may have noticed that these three items seem linked
somehow. Mainly that the last point forces the second which in turn creates a
centralised system. If the code is already on a central server, why not checkout
from there instead of setting up remotes to your team members.

This is currently happening at my work. Slightly over a year ago we migrated
from TFSVC to Git, but we never migrated the process and gave developers to
little time to adapt to this new way of working. So I wanted to facilitate a way
for people to conduct code reviews, with all the fancy features that your
hosting/build/release system has. Enter the judge of all and the fields of bliss: **Minos and Elysium**.

## Key Design Goals ##

With Minos I aim to emphasize the distributive nature of git. With seamless
intergration a developer should allow a diff to be offered up to peers for
review without it clogging up the centralised server (and, depending on your CI
practices, kicking of an entire pipeline).

The project has two distinct parts:

* Minos: The actual interface to create and publish reviews. This repository will
  contain a frontent cli implementing the api.

* Elysium: This centralised system facilitates repository discovery between users. This only syncs review requests between remotes and is not essential to use the tool (The api only looks at remotes, the server will make adding remotes to peers easier).
