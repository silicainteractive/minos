﻿using DiffPlex;
using DiffPlex.DiffBuilder;
using DiffPlex.DiffBuilder.Model;

namespace Minos.Api.Files
{
    public class ChangedFile
    {
        public string FileName { get; set; }
        public DiffPaneModel Diff { get; set; }
        private ChangedFile() { }


        public static ChangedFile Create(string fileName, string a, string b)
        {
            var diffBuilder = new InlineDiffBuilder(new Differ());
            var diff = diffBuilder.BuildDiffModel(a, b);
            return new ChangedFile()
            {
                FileName = fileName,
                Diff = diff
            };
        }
    }
}
