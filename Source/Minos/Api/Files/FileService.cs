using Minos.Api.Core;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System;
using Minos.Api.Utils;

using Newtonsoft.Json;

namespace Minos.Api.Files {
    public class FileService : IFileService
    {
        public void SaveReview(string path, Review review)
        {
            var filepath = path + $"{review.Id}";
            File.WriteAllText(filepath, $"{review.BranchA} -> {review.BranchB}");
        }

        public IEnumerable<BranchPair> LoadReviews(string reviewsPath)
        {
            var branchPairs = new List<BranchPair>();
            var files = Directory.GetFiles(reviewsPath);
            foreach (var file in files)
            {
                if(file.EndsWith(".comment"))
                    continue;
                var line = File.ReadAllText(file);
                var split = line.Trim().Split(new[] { "->" }, StringSplitOptions.RemoveEmptyEntries);
                branchPairs.Add(new BranchPair{
                    From = split[0],
                    To = split[1]
                });
            }
            return branchPairs;
        }
        public IEnumerable<ReviewComment> LoadComments(string reviewsPath){
            var list = new List<ReviewComment>();
            
            var files = Directory
                        .GetFiles(reviewsPath)
                        .Where(f => f.EndsWith(".comment"));

            foreach(var file in files){
                var json = File.ReadAllText(file);
                var comment = JsonConvert.DeserializeObject<ReviewComment>(json);
                list.Add(comment);
            }
            return list;
        }

        public void CreateDirectory(string reviewsPath)
        {
            Directory.CreateDirectory(reviewsPath);
        }

        public void SaveComment(string path, ReviewComment comment)
        {
            var filepath = path + $"{comment.ReviewId}-{comment.Id}.comment";
            var json = JsonConvert.SerializeObject(comment);
            File.WriteAllText(filepath, json);   
        }
    }
}