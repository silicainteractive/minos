﻿namespace Minos.Api.Files
{
    public class FileChanges
    {
        public int DeletedCount { get; set; }
        public int ModifiedCount { get; set; }
        public int AddedCount { get; set; }

    }
}