using System.Collections.Generic;
using Minos.Api.Core;
using Minos.Api.Utils;

namespace Minos.Api.Files{
    public interface IFileService{
        void SaveReview(string path, Review review);
        IEnumerable<BranchPair> LoadReviews(string reviewsPath);
        IEnumerable<ReviewComment> LoadComments(string reviewsPath);
        void CreateDirectory(string reviewsPath);
        void SaveComment(string path, ReviewComment comment);
    }
}