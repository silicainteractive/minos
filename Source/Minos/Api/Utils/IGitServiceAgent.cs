using System.Collections.Generic;
using Minos.Api.Files;

namespace Minos.Api.Utils {
    public interface IGitServiceAgent{
        string User {get;}
        string InfoPath {get;}

        FileChanges GenerateFileChanges(BranchPair branches);
        IEnumerable<ChangedFile> GenerateChangedFiles(BranchPair branches);
    }
}