using System;
using System.Runtime.Serialization;

namespace Minos.Api.Utils
{
    [Serializable]
    public class RepositoryNotValidException : Exception
    {
        public RepositoryNotValidException()
        {
        }

        public RepositoryNotValidException(string message) : base(message)
        {
        }

        public RepositoryNotValidException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected RepositoryNotValidException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}