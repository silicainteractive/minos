namespace Minos.Api.Utils {
    public class BranchPair{
        public string To {get;set;}
        public string From {get;set;}
    }
}