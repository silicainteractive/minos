using System.Collections.Generic;
using System.Linq;
using Minos.Api.Files;
using LibGit2Sharp;

namespace Minos.Api.Utils
{
    public class GitServiceAgent : IGitServiceAgent
    {
        private readonly Repository _repo;

        public string User => $"{_repo.Config.Get<string>("user.Name").Value}";
        public string InfoPath => _repo.Info.Path;

        public GitServiceAgent(string repoLocation){
            var repoRoot = Repository.Discover(repoLocation);
            if(repoRoot == null || !Repository.IsValid(repoRoot))
                throw new RepositoryNotValidException();
            _repo = new Repository(repoLocation);
        }

        public FileChanges GenerateFileChanges(BranchPair branches)
        {
            TreeChanges treeChanges = GetTreeChangesFromBranches(branches);

            return new FileChanges
            {
                DeletedCount = treeChanges.Deleted.Count(),
                AddedCount = treeChanges.Added.Count(),
                ModifiedCount = treeChanges.Modified.Count(),
            };

        }
        public IEnumerable<ChangedFile> GenerateChangedFiles(BranchPair branches)
        {
            var treeChanges = GetTreeChangesFromBranches(branches);

            var diffs = new List<ChangedFile>();
            foreach (var change in treeChanges)
            {
                var obj = _repo.Lookup<Blob>(change.Oid);
                var old = _repo.Lookup<Blob>(change.OldOid);

                var content = obj?.GetContentText() ?? "";
                var Oldcontent = old?.GetContentText() ?? "";

                diffs.Add(ChangedFile.Create(change.Path, content, Oldcontent));
            }

            return diffs;
        }

        private TreeChanges GetTreeChangesFromBranches(BranchPair branches)
        {
            var branchA = _repo.Branches[branches.From];
            var branchB = _repo.Branches[branches.To];
            return _repo.Diff.Compare<TreeChanges>(branchA.Tip.Tree, branchB.Tip.Tree);
        }
    }
}