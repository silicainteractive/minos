using System.Collections.Generic;

namespace Minos.Api.Core {
    public interface IReviewRepository {
        IList<Review> Reviews { get; }
        Review AddReview(string branchA, string branchB);
        ReviewComment AddComment(int reviewId, string content);
    }
}