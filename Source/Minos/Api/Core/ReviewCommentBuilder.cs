﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minos.Api;
using Minos.Api.Utils;

namespace Minos.Api.Core
{
    public class ReviewCommentBuilder
    {
        private string _content;
        private int _id;
        private IGitServiceAgent _serviceAgent;
        private int _reviewId;

        public ReviewCommentBuilder(){
        }

        public ReviewCommentBuilder FromRepository(IGitServiceAgent serviceAgent) {
            _serviceAgent = serviceAgent;
            return this;
        }

        public ReviewCommentBuilder FromReview(Review review){
            _reviewId = review.Id;
            return this;
        }

        public ReviewCommentBuilder WithContent(string content) {
            _content = content;
            return this;
        }

        public ReviewCommentBuilder WithId(int id)
        {
            _id = id;
            return this;
        }

        public ReviewComment Build() {
            return new ReviewComment
            {
                Id = _id,
                ReviewId = _reviewId,
                User = _serviceAgent.User,
                Added = DateTime.Now,
                Content = _content
            };
        }
    }
}
