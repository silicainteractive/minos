﻿using System;
using System.Collections.Generic;
using Minos.Api.Files;

namespace Minos.Api.Core
{
    public class Review
    {
        public IEnumerable<ChangedFile> ChangedFiles { get; internal set; }
        public FileChanges FileChanges{ get; internal set; }
        public string BranchA { get; internal set; }
        public string BranchB { get; internal set; }
        public List<ReviewComment> Comments { get; internal set; }
        public int Id { get; internal set; }

        internal Review() {
            Comments = new List<ReviewComment>();
        }
    }
}
