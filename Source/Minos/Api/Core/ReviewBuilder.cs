﻿using Minos.Api.Files;
using System.Collections.Generic;
using System.Linq;
using System;
using Minos.Api;
using Minos.Api.Utils;

namespace Minos.Api.Core
{
    public class ReviewBuilder
    {
        private IGitServiceAgent _serviceAgent;
        private Queue<string> _branches;
        private int _id;

        public ReviewBuilder()
        {
            _branches = new Queue<string>();
        }
        public ReviewBuilder FromRepository(IGitServiceAgent serviceAgent)
        {
            _serviceAgent = serviceAgent;
            return this;
        }
        public ReviewBuilder WithBranch(string branch)
        {
            _branches.Enqueue(branch);
            return this;
        }

        public ReviewBuilder WithId(int id)
        {
            _id = id;
            return this;
        }

        public Review Build()
        {
            var branches = new BranchPair
            {
                From = _branches.Dequeue(),
                To = _branches.Dequeue(),
            };

            var fileChanges = _serviceAgent.GenerateFileChanges(branches);

            var diffs = _serviceAgent.GenerateChangedFiles(branches);

            return new Review()
            {
                Id = _id,
                BranchA = branches.From,
                BranchB = branches.To,
                FileChanges = fileChanges,
                ChangedFiles = diffs,
            };
        }
    }
}
