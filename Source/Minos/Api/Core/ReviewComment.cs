﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minos.Api.Core
{
    public class ReviewComment
    {
        public string Content { get; set; }
        public string User { get; set; }
        public DateTime Added { get; set; }
        public int Id { get; set; }
        public int ReviewId { get; set; }
    }
}
