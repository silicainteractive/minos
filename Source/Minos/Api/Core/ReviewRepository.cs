﻿using System.Collections.Generic;
using Minos.Api.Utils;
using Minos.Api.Core;
using Minos.Api.Files;

namespace Minos.Api
{
    public class ReviewRepository : IReviewRepository
    {
        public IList<Review> Reviews { get; private set; }
        private readonly IGitServiceAgent _serviceAgent;
        private readonly IFileService _fileService;
        private string ReviewsPath => _serviceAgent.InfoPath + @"reviews\";

        public ReviewRepository(string path) : this(new GitServiceAgent(path), new FileService()) {}

        public ReviewRepository(IGitServiceAgent serviceAgent, IFileService fileService){
            _serviceAgent = serviceAgent;
            _fileService = fileService;
            fileService.CreateDirectory(ReviewsPath);

            LoadReviews();
            LoadComments();
        }

        private void LoadReviews() {
            Reviews = new List<Review>();
            IEnumerable<BranchPair> pairs = _fileService.LoadReviews(ReviewsPath);
            foreach (var branches in pairs)
            {
                AddReview(branches);
            }
        }

        private void LoadComments(){
            var comments = _fileService.LoadComments(ReviewsPath);
            foreach(var comment in comments){
                Reviews[comment.ReviewId].Comments.Add(comment);
            }
        }

        public Review AddReview(string branchA, string branchB) {
            var review = AddReview(new BranchPair{
                From = branchA,
                To = branchB
            });
            
            _fileService.SaveReview(ReviewsPath, review);

            return review;
        }
        private Review AddReview(BranchPair branches){
            var review = new ReviewBuilder()
                .FromRepository(_serviceAgent)
                .WithId(Reviews.Count)
                .WithBranch(branches.From)
                .WithBranch(branches.To)
                .Build();

            Reviews.Add(review);
            return review;
        }

        public ReviewComment AddComment(int reviewId, string content) {
            var review = Reviews[reviewId];
            
            var comment = new ReviewCommentBuilder()
                .FromRepository(_serviceAgent)
                .FromReview(review)
                .WithContent(content)
                .WithId(review.Comments.Count)
                .Build();

            review.Comments.Add(comment);
            _fileService.SaveComment(ReviewsPath, comment);

            return comment;
        }
    }
}
