using Shouldly;
using Xunit;
using Minos.Api.Utils;

public class GitServiceAgentTests{

    [Fact]
    [Trait("RunOnServer", "false")]
    public void ConstructionShouldThrowIfPathIsInvalid()
    {
        Should.Throw(() => {
            new GitServiceAgent("invalidpath");
        }, typeof(RepositoryNotValidException));
    }
}