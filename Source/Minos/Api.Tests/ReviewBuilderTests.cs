using Minos.Api.Core;
using Minos.Api.Utils;
using Moq;
using Shouldly;
using Xunit;

namespace Minos.Api.Tests {

    public class ReviewBuilderTests{

        [Fact]
        public void ReviewBuilderShouldReturnReviewWithAttributes(){
            var serviceAgentMock = new Mock<IGitServiceAgent>();
            
            var _sut = new ReviewBuilder();
            var review = _sut
                .FromRepository(serviceAgentMock.Object)
                .WithBranch("branchA")
                .WithBranch("branchB")
                .WithId(0)
                .Build();
            
            review.BranchA.ShouldBe("branchA");
            review.BranchB.ShouldBe("branchB");
            review.Id.ShouldBe(0);
        }
    }
}