using Minos.Api.Files;
using Minos.Api.Core;
using Minos.Api.Utils;
using Moq;
using Shouldly;
using Xunit;

namespace Minos.Api.Tests {
    public class ReviewRepositoryTests {
        private readonly Mock<IGitServiceAgent> _serviceAgent;
        private readonly Mock<IFileService> _fileService;

        public ReviewRepositoryTests(){
            _serviceAgent = new Mock<IGitServiceAgent>();
            _fileService = new Mock<IFileService>();
        }

        private ReviewRepository CreateSut() 
            => new ReviewRepository(_serviceAgent.Object, _fileService.Object);

        [Fact]
        public void AddReviewShouldGenerateAndSaveReview(){
            var sut = CreateSut();
            var review = sut.AddReview("from", "to");

            _fileService.Verify(x => 
                x.SaveReview(
                    It.IsAny<string>(),
                    It.Is<Review>(r => 
                        r.BranchA == "from" &&
                        r.BranchB == "to")
                    )
            );

            _serviceAgent.Verify( x => 
                x.GenerateChangedFiles(It.IsAny<BranchPair>())
            );
            _serviceAgent.Verify( x => 
                x.GenerateFileChanges(It.IsAny<BranchPair>())
            );

            review.Id.ShouldBe(0);
            review.BranchA.ShouldBe("from");
            review.BranchB.ShouldBe("to");
        }

        [Fact]
        public void AddCommentShouldGenerateAndSaveComment(){
            var sut = CreateSut();

            HasReview(sut);

            _serviceAgent.Setup(s => s.User).Returns("Test reviewer");
            var comment = sut.AddComment(0, "someContent");

            _fileService.Verify(x => 
                x.SaveComment(
                    It.IsAny<string>(),
                    It.Is<ReviewComment>(c => 
                        c.Content == "someContent" &&
                        c.ReviewId == 0 &&
                        c.User == "Test reviewer")
                    )
            );
        }

        [Fact]
        public void LoadReviewsShouldGenerateAndNotSaveReview(){
            var sut = CreateSut();

            _fileService.Verify(x => x.LoadReviews(It.IsAny<string>()));
            _fileService.Verify(x => x.LoadComments(It.IsAny<string>()));
            _fileService.Verify(x => x.SaveReview(It.IsAny<string>(), It.IsAny<Review>()), Times.Never);
        }

        private void HasReview(ReviewRepository repository){
            HasReview(repository, "from", "to");
        }
        private void HasReview(ReviewRepository repository, string branchFrom, string branchTo){
            repository.AddReview(branchFrom, branchTo);
        }
    }
}