public class Tokenizer {
    public void Tokenize(string input){

    }
}

public class Token {
    TokenType type;
    string value;
}

public enum TokenType {
    Command,
    Argument,
    Option
}

public class TokenMatch {
    public bool IsMatch{get;set;}
    public TokenType TokenType { get; set; }
    public string Value { get; set; }
    public string RemainingText { get; set; }
}
