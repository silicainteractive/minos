

using System.Text.RegularExpressions;

public class TokenDefinition {
    private readonly Regex _regex;

    public TokenDefinition(string regex){
        _regex = new Regex(regex, RegexOptions.IgnoreCase);
    }
}