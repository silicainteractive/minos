using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Elysium;
using Elysium.Models;
using Elysium.Controllers;
public class RepositoryControllerTests{

    [Fact]
    public void GetShouldReturnOkObjectResult(){
        using(var context = CreateContext()){
            var testRepo = HasRepository(context);
            var testOwner = HasOwnerWithRepositories(context, new [] {testRepo});

            var sut = new RepositoryController(context);
            var result = sut.Get(testOwner.Name,testRepo.RepositoryName).Result;

            Assert.IsType<OkObjectResult>(result);
         }
    }

    [Fact]
    public void GetShouldReturnNotFoundIfOwnerDoesNotExist(){
        using(var context = CreateContext()){
            var testRepo = HasRepository(context);

            var sut = new RepositoryController(context);
            var result = sut.Get("testOwner",testRepo.RepositoryName).Result;

            Assert.IsType<NotFoundObjectResult>(result);
         }
    }

    [Fact]
    public void GetShouldReturnNotFoundIfRepositoryDoesNotExist(){
        using(var context = CreateContext()){
            var testOwner =  HasOwner(context);

            var sut = new RepositoryController(context);
            var result = sut.Get(testOwner.Name, "testRepo").Result;

            Assert.IsType<NotFoundObjectResult>(result);
         }
    }

    [Fact]
    public void CreateShouldReturnCreatedAtResult(){
        using(var context = CreateContext()){
            var testRepo = new Repository {
                Path = "c://testrepo",
                RepositoryName = "TestRepo"
            };
            var testOwner = HasOwner(context);

            var sut = new RepositoryController(context);
            var result = sut.Create(testOwner.Name, testRepo).Result;

            Assert.IsType<CreatedAtRouteResult>(result);
         }
    }

    private ReferenceContext CreateContext(){
        var options = new DbContextOptionsBuilder<ReferenceContext>()
            .UseInMemoryDatabase(Guid.NewGuid().ToString())
            .Options;
        
        return new ReferenceContext(options);
    }
    private Repository HasRepository(ReferenceContext context) {
        var repo = new Repository {
                Path = "c://testrepo",
                RepositoryName = "TestRepo"
        };
        context.Repositories.Add(repo);
        context.SaveChanges();
        return repo;
    }
    private Owner HasOwnerWithRepositories(ReferenceContext context, IEnumerable<Repository> repositories){
        var owner = new Owner{
            Name="TestOwner",
            Repositories = new List<Repository>(repositories)
        };
        context.Owners.Add(owner);
        context.SaveChanges();
        return owner;
    }
    private Owner HasOwner(ReferenceContext context){
        var owner = new Owner{
            Name="TestOwner"
        };
        context.Owners.Add(owner);
        context.SaveChanges();
        return owner;
    }
}