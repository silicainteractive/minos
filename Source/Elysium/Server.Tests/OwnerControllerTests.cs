using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Elysium;
using Elysium.Models;
using Elysium.Controllers;
public class OwnerControllerTests {

    [Fact]
    public void GetShouldReturnOwner(){
        using(var context = CreateContext()){
            context.Owners.Add(new Owner {Name = "TestOwner"});
            context.SaveChanges();

            var sut = new OwnerController(context);
            var result = sut.Get("TestOwner").Result;

            Assert.IsType<OkObjectResult>(result);
        }
    }

    [Fact]
    public void GetShouldReturnAllOwners(){
        using(var context = CreateContext()){
            var owner1 = new Owner {Name = "TestOwner1"};
            var owner2 = new Owner {Name = "TestOwner2"};
            var owner3 = new Owner {Name = "TestOwner3"};
            context.AddRange(owner1, owner2, owner3);

            context.SaveChanges();

            var sut = new OwnerController(context);
            var result = sut.Get().Result;

            Assert.IsType<OkObjectResult>(result);
            var typedResult = ((OkObjectResult)result).Value as List<Owner>;
            Assert.NotNull(typedResult);
            Assert.Contains(owner1, typedResult);
            Assert.Contains(owner2, typedResult);
            Assert.Contains(owner3, typedResult);
        }
    }

    [Fact]
    public void GetShouldReturnNotFound(){
        using(var context = CreateContext()){
            var sut = new OwnerController(context);
            var result = sut.Get("TestOwner").Result;

            Assert.IsType<NotFoundObjectResult>(result);
        }
    }

    [Fact]
    public void CreateShouldReturnCreatedAtResult(){
        using(var context = CreateContext()){
            var owner = new Owner {Name = "testOwner"};

            var sut = new OwnerController(context);
            var result = sut.Create(owner).Result;

            Assert.IsType<CreatedAtRouteResult>(result);
            Assert.Contains(owner, context.Owners);
        }
    }
    [Fact]
    public void CreateShouldReturnBadRequest(){
        using(var context = CreateContext()){
            var owner = new Owner {Name = "testOwner"};
            context.Owners.AddAsync(owner);

            var sut = new OwnerController(context);
            var result = sut.Create(owner).Result;

            Assert.IsType<BadRequestObjectResult>(result);
        }
    }

    private ReferenceContext CreateContext(){
        var options = new DbContextOptionsBuilder<ReferenceContext>()
            .UseInMemoryDatabase(Guid.NewGuid().ToString())
            .Options;
        
        return new ReferenceContext(options);
    }
}