using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Elysium.Models
{
    public class Owner
    {
        [Key]
        public string Name { get; set; }
        public List<Repository> Repositories { get; set; }

        public Owner()
        {
            Repositories = new List<Repository>();
        }
    }
}