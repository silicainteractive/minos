using System.ComponentModel.DataAnnotations;

namespace Elysium.Models
{
    public class Repository
    {
        [Key]
        public int Id { get; set; }
        public string RepositoryName { get; set; }
        public string Path { get; set; }
    }
}