using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Elysium.Models;

namespace Elysium
{
    public class ReferenceContext : DbContext
    {
        public DbSet<Repository> Repositories { get; set; }
        public DbSet<Owner> Owners { get; set; }

        public ReferenceContext(DbContextOptions<ReferenceContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Repository>()
                .HasAlternateKey(r => r.RepositoryName)
                .HasName("AlternateKey_Repository");
        }

    }
}