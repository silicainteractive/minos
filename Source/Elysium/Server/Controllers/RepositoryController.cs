using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Elysium.Models;

namespace Elysium.Controllers
{
    public class RepositoryController : Controller
    {

        private readonly ReferenceContext _context;

        public RepositoryController(ReferenceContext context)
        {
            _context = context;
        }

        [HttpGet("api/[controller]/{ownerName}/{repoName}", Name = "GetRepo")]
        public async Task<IActionResult> Get(string ownerName, string repoName)
        {
            var owner = await GetOwnerFromContext(ownerName);
            if (owner == null)
                return NotFound($"Owner {ownerName} does not exist");

            var repo = owner.Repositories.FirstOrDefault(r => r.RepositoryName == repoName);
            if (repo == null)
                return NotFound($"Repository {repoName} not found registered to owner {ownerName}");

            return new OkObjectResult(repo);
        }

        [HttpPost("api/[controller]/{ownerName}/")]
        public async Task<IActionResult> Create(string ownerName, [FromBody]Repository repo)
        {
            var owner = await GetOwnerFromContext(ownerName);
            if (owner == null)
                return NotFound($"Owner {ownerName} does not exist");

            if (owner.Repositories.Any(r => r.RepositoryName == repo.RepositoryName))
            {
                return BadRequest($"Repository {repo.RepositoryName} already registered to owner {ownerName}");
            }

            await _context.Repositories.AddAsync(repo);
            _context.Update(owner);
            owner.Repositories.Add(repo);
            await _context.SaveChangesAsync();

            return CreatedAtRoute("GetRepo", new { ownerName = ownerName, repoName = repo.RepositoryName }, repo);
        }

        private async Task<Owner> GetOwnerFromContext(string ownerName)
            => await _context.Owners.Include(o => o.Repositories).FirstOrDefaultAsync(o => o.Name == ownerName);

    }
}