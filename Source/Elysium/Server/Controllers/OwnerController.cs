using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Elysium.Models;

namespace Elysium.Controllers
{
    public class OwnerController : Controller
    {

        private readonly ReferenceContext _context;

        public OwnerController(ReferenceContext context)
        {
            _context = context;
        }

        [HttpGet("api/[controller]/", Name = "GetAll")]
        public async Task<IActionResult> Get()
        {
            var all = await _context.Owners.ToListAsync();
            return new OkObjectResult(all);
        }

        [HttpGet("api/[controller]/{ownerName}", Name = "GetOwner")]
        public async Task<IActionResult> Get(string ownerName)
        {
            var owner = await _context.Owners.Include(o => o.Repositories).FirstOrDefaultAsync(o => o.Name == ownerName);
            if (owner == null)
                return NotFound($"Owner {ownerName} not found");

            return new OkObjectResult(owner);
        }

        [HttpPost("api/[controller]/")]
        public async Task<IActionResult> Create([FromBody] Owner owner)
        {
            var exists = await _context.Owners.FindAsync(owner.Name);
            if (exists != null)
                return BadRequest($"Owner {owner.Name} already exists");

            await _context.Owners.AddAsync(owner);
            await _context.SaveChangesAsync();

            return CreatedAtRoute("GetOwner", new { ownerName = owner.Name }, owner);
        }
    }
}